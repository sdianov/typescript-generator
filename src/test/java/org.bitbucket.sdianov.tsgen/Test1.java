package org.bitbucket.sdianov.tsgen;

import org.bitbucket.sdianov.tsgen.elements.TSClass;
import org.bitbucket.sdianov.tsgen.elements.TSConst;
import org.bitbucket.sdianov.tsgen.elements.TSField;
import org.bitbucket.sdianov.tsgen.elements.TSInterface;
import org.bitbucket.sdianov.tsgen.elements.TSMethod;
import org.bitbucket.sdianov.tsgen.elements.TSParameter;
import org.bitbucket.sdianov.tsgen.elements.TSUnit;
import org.bitbucket.sdianov.tsgen.expressions.TSInterpolatedString;
import org.bitbucket.sdianov.tsgen.expressions.TSMethodCall;
import org.bitbucket.sdianov.tsgen.operators.TSReturn;
import org.bitbucket.sdianov.tsgen.renderer.DefaultRenderer;
import org.bitbucket.sdianov.tsgen.renderer.Renderer;
import org.bitbucket.sdianov.tsgen.types.ArrayType;
import org.bitbucket.sdianov.tsgen.types.GenericType;
import org.bitbucket.sdianov.tsgen.types.SimpleType;
import org.bitbucket.sdianov.tsgen.types.StandardTypes;
import org.junit.Test;

public class Test1 {

    @Test
    public void test1() {

        TSUnit unit1 = new TSUnit();

        GenericType observableResponse = new GenericType("Observable", new SimpleType("Response"));
        observableResponse.setModuleName("rxjs/Observable");

        unit1.addImport("@angular/http", "Http", "Response");
        unit1.addImport("rxjs/Observable", "Observable");
        unit1.addImport("@angular/core", "Injectable");

        TSConst base = unit1.addConst("BASE_URL", "'https://site.org'");

        TSInterface interface1 = unit1.addInterface("IParameters");

        TSField id = interface1.addField("id", StandardTypes.NUMBER, false);
        interface1.addField("name", StandardTypes.STRING, false);
        interface1.addField("roles", new ArrayType(StandardTypes.STRING), true);

        TSClass class1 = unit1.addClass("ServiceClient");

        class1.addConstructor(
                new TSParameter("_http", new SimpleType("Http")).withModifiers("private")
        );
        class1.addDecorator("Injectable");

        TSMethod method = class1.addMethod("callService",
                observableResponse,
                new TSParameter("params", interface1.getType())
        );

        method.getBody().addOperator(
                new TSReturn(
                        new TSMethodCall(
                                "this._http",
                                "get",
                                new TSInterpolatedString("%s/api/service/%s",
                                        base.getName(), "params.id") //"`${BASE_URL}/api/service/${params.id}`"
                        )
                )
        );

        Renderer<TSUnit> renderer = new DefaultRenderer();

        String lines = renderer.render(unit1);

        System.out.println(lines);

    }
}
