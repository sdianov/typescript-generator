package org.bitbucket.sdianov.tsgen.renderer;

import org.bitbucket.sdianov.tsgen.types.ArrayType;
import org.bitbucket.sdianov.tsgen.types.GenericType;
import org.bitbucket.sdianov.tsgen.types.SimpleType;
import org.bitbucket.sdianov.tsgen.types.TSType;
import org.bitbucket.sdianov.tsgen.types.UnionType;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static org.bitbucket.sdianov.tsgen.utils.RenderUtils.commaJoin;
import static org.bitbucket.sdianov.tsgen.utils.RenderUtils.pipeJoin;
import static org.bitbucket.sdianov.tsgen.utils.RenderUtils.triangleBrace;

public class TypesRenderer implements Renderer<TSType> {

    private Map<Class<? extends TSType>, Function<TSType, String>> renderMap = new HashMap<>();

    @SuppressWarnings("unchecked")
    private <T extends TSType> void register(Class<T> clazz, Function<T, String> renderFunc) {
        renderMap.put(clazz, (Function) renderFunc);
    }

    public TypesRenderer() {

        register(SimpleType.class, SimpleType::getTypeName);

        register(ArrayType.class, arrayType ->
                this.render(arrayType.getElementType()) + "[]");

        register(GenericType.class, genericType ->
                genericType.getName() + triangleBrace(commaJoin(genericType.getParameters(), this::render)));

        register(UnionType.class, unionType ->
                pipeJoin(unionType.getElements(), this::render));
    }

    public String render(TSType type) {
        return renderMap.getOrDefault(type.getClass(), x -> {
            throw new IllegalArgumentException("Invalid type: " + x);
        }).apply(type);
    }
}