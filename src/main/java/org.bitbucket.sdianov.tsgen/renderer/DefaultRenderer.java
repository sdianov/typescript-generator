package org.bitbucket.sdianov.tsgen.renderer;

import org.bitbucket.sdianov.tsgen.elements.TSClass;
import org.bitbucket.sdianov.tsgen.elements.TSConst;
import org.bitbucket.sdianov.tsgen.elements.TSConstructor;
import org.bitbucket.sdianov.tsgen.elements.TSContainer;
import org.bitbucket.sdianov.tsgen.elements.TSDecorator;
import org.bitbucket.sdianov.tsgen.elements.TSElement;
import org.bitbucket.sdianov.tsgen.elements.TSField;
import org.bitbucket.sdianov.tsgen.elements.TSImport;
import org.bitbucket.sdianov.tsgen.elements.TSInterface;
import org.bitbucket.sdianov.tsgen.elements.TSMethod;
import org.bitbucket.sdianov.tsgen.elements.TSUnit;
import org.bitbucket.sdianov.tsgen.utils.IndentWriter;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

import static org.bitbucket.sdianov.tsgen.utils.RenderUtils.commaJoin;
import static org.bitbucket.sdianov.tsgen.utils.RenderUtils.spaceJoin;

public class DefaultRenderer implements Renderer<TSUnit> {

    private Map<Class<? extends TSElement>, Consumer<TSElement>> renderMap = new HashMap<>();

    @SuppressWarnings("unchecked")
    private <T extends TSElement> void register(Class<T> clazz, Consumer<T> renderFunc) {
        renderMap.put(clazz, (Consumer) renderFunc);
    }

    private TypesRenderer typesRenderer = new TypesRenderer();

    private StringBuilder builder = new StringBuilder();

    private IndentWriter writer = new IndentWriter(builder);

    private final OperatorRenderer operatorRenderer = new OperatorRenderer(writer);

    public DefaultRenderer() {

        register(TSClass.class, tsClass -> {
            writer.writeBlockStart("export class %s {", tsClass.getName());
            renderContainer(tsClass);
            writer.writeBlockEnd("}");
        });

        register(TSInterface.class, tsInterface -> {
            writer.writeBlockStart("export interface %s {", tsInterface.getName());
            renderContainer(tsInterface);
            writer.writeBlockEnd("}");
        });

        register(TSField.class, tsField -> {
            writer.writeLine("%s%s: %s;",
                    tsField.getName(),
                    tsField.isOptional() ? "?" : "",
                    typesRenderer.render(tsField.getType()));
        });

        register(TSMethod.class, tsMethod -> {

            String paramList = commaJoin(tsMethod.getParameters(), param ->
                    param.getName() + ": " + typesRenderer.render(param.getType()));

            writer.writeBlockStart(tsMethod.getName() + "(" + paramList + ")"
                    + ": " + typesRenderer.render(tsMethod.getReturnType()) + " {");

            operatorRenderer.render(tsMethod.getBody());

            writer.writeBlockEnd("}");
        });

        register(TSConstructor.class, tsConstructor -> {

            String paramList = commaJoin(tsConstructor.getParameters(),
                    param -> spaceJoin(param.getModifiers(), Function.identity()) + " "
                            + param.getName() + ": " + typesRenderer.render(param.getType()));

            writer.writeBlockStart("constructor (" + paramList + ") {");

            writer.writeBlockEnd("}");
        });

        register(TSImport.class, tsImport -> {
            writer.writeLine("import {" + commaJoin(tsImport.getTypeNames(), Function.identity())
                    + "} from '" + tsImport.getModule() + "';");
        });

        register(TSConst.class, tsConst -> {
            writer.writeLine("export const %s=%s;", tsConst.getName(), tsConst.getExpression());
        });

    }

    @Override
    public String render(TSUnit unit) {

        for (TSElement element : unit.getElements()) {
            renderElement(element);
        }
        return builder.toString();
    }

    private void renderContainer(TSContainer container) {
        for (TSElement element : container.getElements()) {
            renderElement(element);
        }
    }

    private void renderElement(TSElement element) {

        for (TSDecorator decorator : element.getDecorators()) {
            writer.writeLine("@%s()", decorator.getName());
        }

        renderMap.getOrDefault(element.getClass(),
                e -> {
                    throw new IllegalArgumentException("Invalid element " + element);
                }
        ).accept(element);

    }
}
