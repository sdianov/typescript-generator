package org.bitbucket.sdianov.tsgen.renderer;

import org.bitbucket.sdianov.tsgen.operators.TSCodeBlock;
import org.bitbucket.sdianov.tsgen.operators.TSOperator;
import org.bitbucket.sdianov.tsgen.operators.TSReturn;
import org.bitbucket.sdianov.tsgen.utils.IndentWriter;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class OperatorRenderer {

    private final IndentWriter writer;

    private Map<Class<? extends TSOperator>, Consumer<TSOperator>> renderMap = new HashMap<>();

    private ExpressionRenderer expressionRenderer = new ExpressionRenderer();

    public OperatorRenderer(IndentWriter writer) {

        this.writer = writer;

        register(TSReturn.class, tsReturn -> {
            writer.writeLine("return %s",
                    expressionRenderer.render(tsReturn.getExpression()));
        });
    }

    @SuppressWarnings("unchecked")
    private <T extends TSOperator> void register(Class<T> clazz, Consumer<T> renderFunc) {
        renderMap.put(clazz, (Consumer) renderFunc);
    }

    public void render(TSCodeBlock codeBlock) {

        for (TSOperator operator : codeBlock.getOperators()) {
            renderOperator(operator);

        }
    }

    private void renderOperator(TSOperator operator) {
        renderMap.getOrDefault(operator.getClass(), x -> {
                    throw new IllegalArgumentException("Invalid operator: " + operator);
                }
        ).accept(operator);
    }
}
