package org.bitbucket.sdianov.tsgen.renderer;

import org.bitbucket.sdianov.tsgen.elements.TSUnit;

public interface Renderer<T> {

    String render(T unit);

}
