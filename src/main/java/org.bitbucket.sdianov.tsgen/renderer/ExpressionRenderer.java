package org.bitbucket.sdianov.tsgen.renderer;

import org.bitbucket.sdianov.tsgen.expressions.TSExpression;
import org.bitbucket.sdianov.tsgen.expressions.TSInterpolatedString;
import org.bitbucket.sdianov.tsgen.expressions.TSMethodCall;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.bitbucket.sdianov.tsgen.utils.RenderUtils.commaJoin;

public class ExpressionRenderer {

    private Map<Class<? extends TSExpression>, Function<TSExpression, String>> renderMap = new HashMap<>();

    @SuppressWarnings("unchecked")
    private <T extends TSExpression> void register(Class<T> clazz, Function<T, String> renderFunc) {
        renderMap.put(clazz, (Function) renderFunc);
    }

    public ExpressionRenderer() {

        register(TSMethodCall.class, tsMethodCall ->
                String.format("%s.%s(%s)",
                        tsMethodCall.getObject(),
                        tsMethodCall.getMethod(),
                        commaJoin(tsMethodCall.getParameters(), this::render))
        );

        register(TSInterpolatedString.class, tsString -> {
            return "`" + String.format(tsString.getFormat(),
                    tsString.getParameters().stream().map(x -> "${" + x +"}").toArray()) + "`";
        });
    }

    public String render(TSExpression expression) {

        return renderMap.getOrDefault(expression.getClass(), x -> {
                    throw new IllegalArgumentException("Expression is invalid: " + expression);
                }
        ).apply(expression);
    }

}
