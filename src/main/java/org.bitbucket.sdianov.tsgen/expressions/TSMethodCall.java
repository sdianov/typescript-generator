package org.bitbucket.sdianov.tsgen.expressions;

import java.util.Arrays;
import java.util.List;

public class TSMethodCall implements TSExpression {

    private final String object;

    private final String method;

    private final List<TSExpression> parameters;

    public TSMethodCall(String object, String method, TSExpression... parameters) {
        this.object = object;
        this.method = method;
        this.parameters = Arrays.asList(parameters);
    }

    public String getObject() {
        return object;
    }

    public String getMethod() {
        return method;
    }

    public List<TSExpression> getParameters() {
        return parameters;
    }

}
