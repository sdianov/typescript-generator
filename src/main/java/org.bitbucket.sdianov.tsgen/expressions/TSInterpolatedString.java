package org.bitbucket.sdianov.tsgen.expressions;

import java.util.Arrays;
import java.util.List;

public class TSInterpolatedString implements TSExpression {

    private final String format;

    private final List<String> parameters;

    public TSInterpolatedString(String format, String... parameters) {
        this.format = format;
        this.parameters = Arrays.asList(parameters);
    }

    public String getFormat() {
        return format;
    }

    public List<String> getParameters() {
        return parameters;
    }
}
