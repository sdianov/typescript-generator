package org.bitbucket.sdianov.tsgen.expressions;

import org.bitbucket.sdianov.tsgen.types.TSType;

public class TSTypeAssertion {

    private final TSExpression expression;

    private final TSType type;

    public TSTypeAssertion(TSExpression expression, TSType type) {
        this.expression = expression;
        this.type = type;
    }

    public TSExpression getExpression() {
        return expression;
    }

    public TSType getType() {
        return type;
    }
}
