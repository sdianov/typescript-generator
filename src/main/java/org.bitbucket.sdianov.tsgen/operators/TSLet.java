package org.bitbucket.sdianov.tsgen.operators;

import org.bitbucket.sdianov.tsgen.expressions.TSExpression;

public class TSLet {

    public final String name;

    public final TSExpression expression;

    public TSLet(String name, TSExpression expression) {
        this.name = name;
        this.expression = expression;
    }

    public String getName() {
        return name;
    }

    public TSExpression getExpression() {
        return expression;
    }

}
