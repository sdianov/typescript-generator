package org.bitbucket.sdianov.tsgen.operators;

import java.util.ArrayList;
import java.util.List;

public class TSCodeBlock implements TSOperator {

    private final List<TSOperator> operators = new ArrayList<>();

    public void addOperator(TSOperator operator) {
        operators.add(operator);
    }

    public List<TSOperator> getOperators() {
        return operators;
    }
}
