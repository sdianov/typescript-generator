package org.bitbucket.sdianov.tsgen.operators;

import org.bitbucket.sdianov.tsgen.expressions.TSExpression;

public class TSReturn implements TSOperator {

    private final TSExpression expression;

    public TSReturn(TSExpression expression) {
        this.expression = expression;
    }

    public TSExpression getExpression() {
        return expression;
    }
}
