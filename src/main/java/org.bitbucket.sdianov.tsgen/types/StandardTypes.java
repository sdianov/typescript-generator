package org.bitbucket.sdianov.tsgen.types;

public class StandardTypes {

    public final static SimpleType NUMBER = new SimpleType("number");
    public final static SimpleType STRING = new SimpleType("string");
    public final static SimpleType BOOLEAN = new SimpleType("boolean");

    public final static SimpleType ANY = new SimpleType("any");
    public final static SimpleType OBJECT = new SimpleType("object");

    public final static SimpleType VOID = new SimpleType("void");
    public final static SimpleType NULL = new SimpleType("null");
    public final static SimpleType UNDEFINED = new SimpleType("undefined");
    public final static SimpleType NEVER = new SimpleType("never");


}