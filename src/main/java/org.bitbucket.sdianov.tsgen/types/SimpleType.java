package org.bitbucket.sdianov.tsgen.types;

public class SimpleType extends TSType {

    private final String typeName;

    public SimpleType(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeName() {
        return typeName;
    }
}
