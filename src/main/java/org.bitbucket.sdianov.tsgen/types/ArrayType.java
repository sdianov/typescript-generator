package org.bitbucket.sdianov.tsgen.types;

public class ArrayType extends TSType {

    private final SimpleType elementType;

    public ArrayType(SimpleType elementType) {
        this.elementType = elementType;
    }

    public SimpleType getElementType() {
        return elementType;
    }
}
