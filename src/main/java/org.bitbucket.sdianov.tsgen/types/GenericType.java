package org.bitbucket.sdianov.tsgen.types;

import java.util.Arrays;
import java.util.List;

public class GenericType extends TSType {

    private final String name;

    private final List<TSType> parameters;

    public GenericType(String name, TSType... parameters) {
        this.name = name;
        this.parameters = Arrays.asList(parameters);
    }

    public String getName() {
        return name;
    }

    public List<TSType> getParameters() {
        return parameters;
    }

}
