package org.bitbucket.sdianov.tsgen.types;

import java.util.List;

public class EnumType extends TSType {

    private final String name;

    private List<String> elements;

    public EnumType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<String> getElements() {
        return elements;
    }
}
