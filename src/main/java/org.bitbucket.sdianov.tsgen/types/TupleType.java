package org.bitbucket.sdianov.tsgen.types;

import java.util.Arrays;
import java.util.List;

public class TupleType extends TSType {

    private final List<TSType> parameters;

    public TupleType(TSType... parameters) {
        this.parameters = Arrays.asList(parameters);
    }

    public List<TSType> getParameters() {
        return parameters;
    }
}
