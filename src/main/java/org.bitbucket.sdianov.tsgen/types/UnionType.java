package org.bitbucket.sdianov.tsgen.types;

import java.util.List;

public class UnionType extends TSType {

    private final List<TSType> elements;

    public UnionType(List<TSType> elements) {
        this.elements = elements;
    }

    public List<TSType> getElements() {
        return elements;
    }
}
