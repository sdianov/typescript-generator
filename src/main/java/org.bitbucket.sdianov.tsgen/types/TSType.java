package org.bitbucket.sdianov.tsgen.types;

public abstract class TSType {

    private String moduleName;

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }
}
