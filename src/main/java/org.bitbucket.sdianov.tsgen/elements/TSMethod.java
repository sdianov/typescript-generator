package org.bitbucket.sdianov.tsgen.elements;


import org.bitbucket.sdianov.tsgen.operators.TSCodeBlock;
import org.bitbucket.sdianov.tsgen.types.TSType;

import java.util.Arrays;
import java.util.List;

public class TSMethod extends TSNamedElement {

    private final TSCodeBlock body;
    private final TSType returnType;

    private List<TSParameter> parameters;

    TSMethod(TSContainer parent, String name, TSType returnType, TSParameter... parameters) {
        super(parent, name);
        body = new TSCodeBlock();
        this.returnType = returnType;
        this.parameters = Arrays.asList(parameters);
    }

    public TSCodeBlock getBody() {
        return body;
    }

    public TSType getReturnType() {
        return returnType;
    }

    public List<TSParameter> getParameters() {
        return parameters;
    }
}
