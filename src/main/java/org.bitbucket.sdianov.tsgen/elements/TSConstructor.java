package org.bitbucket.sdianov.tsgen.elements;

import java.util.Arrays;
import java.util.List;

public class TSConstructor extends TSElement {

    private final List<TSParameter> parameters;

    TSConstructor(TSContainer parent, TSParameter ... parameters) {
        super(parent);
        this.parameters = Arrays.asList(parameters);
    }

    public List<TSParameter> getParameters() {
        return parameters;
    }
}
