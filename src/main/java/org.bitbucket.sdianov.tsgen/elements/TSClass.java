package org.bitbucket.sdianov.tsgen.elements;

import org.bitbucket.sdianov.tsgen.types.TSType;

public class TSClass extends TSContainer {

    TSClass(TSContainer parent, String name) {
        super(parent, name);
    }

    public TSMethod addMethod(String name, TSType returnType, TSParameter... parameters) {
        TSMethod result = new TSMethod(this, name, returnType, parameters);

        elements.add(result);
        return result;
    }

    public TSConstructor addConstructor(TSParameter ... parameters){
        TSConstructor result = new TSConstructor(this, parameters);

        elements.add(result);
        return result;
    }
}
