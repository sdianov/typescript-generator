package org.bitbucket.sdianov.tsgen.elements;

import com.google.common.base.Strings;
import org.bitbucket.sdianov.tsgen.types.TSType;

import java.util.Arrays;
import java.util.List;

public class TSImport extends TSElement {

    private final String module;

    private final List<String> typeNames;

    TSImport(TSContainer parent, String module, String... typeNames) {
        super(parent);
        this.module = module;
        this.typeNames = Arrays.asList(typeNames);
    }

    public String getModule() {
        return module;
    }

    public List<String> getTypeNames() {
        return typeNames;
    }

}
