package org.bitbucket.sdianov.tsgen.elements;

import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.List;

public abstract class TSContainer extends TSNamedElement {

    TSContainer(TSContainer parent, String name) {
        super(parent, name);
    }

    protected List<TSElement> elements = new ArrayList<>();

    public List<TSElement> getElements() {
        return ImmutableList.copyOf(elements);
    }

}

