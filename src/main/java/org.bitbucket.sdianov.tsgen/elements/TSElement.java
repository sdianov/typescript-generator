package org.bitbucket.sdianov.tsgen.elements;

import java.util.ArrayList;
import java.util.List;

public abstract class TSElement {

    private final TSContainer parent;

    private final List<TSDecorator> decorators = new ArrayList<>();

    TSElement(TSContainer parent) {
        this.parent = parent;
    }

    public void addDecorator(String name) {
        decorators.add(new TSDecorator(name));
    }

    public List<TSDecorator> getDecorators() {
        return decorators;
    }
}
