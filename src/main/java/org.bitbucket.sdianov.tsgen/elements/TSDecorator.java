package org.bitbucket.sdianov.tsgen.elements;

public class TSDecorator {

    private final String name;

    TSDecorator( String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
