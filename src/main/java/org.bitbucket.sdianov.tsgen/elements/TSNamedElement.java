package org.bitbucket.sdianov.tsgen.elements;

public class TSNamedElement extends TSElement {

    private final String name;

    TSNamedElement(TSContainer parent, String name) {
        super(parent);
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
