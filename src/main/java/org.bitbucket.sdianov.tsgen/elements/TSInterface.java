package org.bitbucket.sdianov.tsgen.elements;

import org.bitbucket.sdianov.tsgen.types.SimpleType;
import org.bitbucket.sdianov.tsgen.types.TSType;

public class TSInterface extends TSContainer {

    TSInterface(TSContainer parent, String name){
        super(parent, name);
    }

    public TSField addField(String name, TSType type, boolean optional) {
        TSField result = new TSField(this, name, type, optional);

        elements.add(result);
        return result;
    }

    public TSType getType() {
        return new SimpleType(getName());
    }
}
