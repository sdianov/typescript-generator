package org.bitbucket.sdianov.tsgen.elements;

public class TSConst extends TSNamedElement {

    private final String expression;

    TSConst(TSContainer parent, String name, String expression) {
        super(parent, name);
        this.expression = expression;
    }

    public String getExpression() {
        return expression;
    }
}
