package org.bitbucket.sdianov.tsgen.elements;

import org.bitbucket.sdianov.tsgen.operators.TSCodeBlock;

import java.util.Arrays;
import java.util.List;

public class TSFunction extends TSNamedElement {

    private final List<TSParameter> parameters;

    private TSCodeBlock body;

    TSFunction(TSContainer parent, String name, TSParameter ... parameters) {
        super(parent, name);
        this.parameters = Arrays.asList(parameters);
        body = new TSCodeBlock();
    }

    public List<TSParameter> getParameters() {
        return parameters;
    }

    public TSCodeBlock getBody() {
        return body;
    }
}
