package org.bitbucket.sdianov.tsgen.elements;

import org.bitbucket.sdianov.tsgen.types.TSType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TSParameter {

    private final String name;

    private final TSType type;

    private final List<String> modifiers = new ArrayList<>();

    public TSParameter(String name, TSType type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public TSType getType() {
        return type;
    }

    public TSParameter withModifiers(String... modifiers) {
        this.modifiers.addAll(Arrays.asList(modifiers));
        return this;
    }

    public List<String> getModifiers() {
        return modifiers;
    }
}

