package org.bitbucket.sdianov.tsgen.elements;

import org.bitbucket.sdianov.tsgen.types.TSType;

public class TSField extends TSNamedElement {

    private final TSType type;

    private final boolean optional;

    TSField(TSContainer parent, String name, TSType type, boolean optional){
        super(parent, name);
        this.type = type;
        this.optional = optional;
    }

    public TSType getType() {
        return type;
    }

    public boolean isOptional() {
        return optional;
    }
}
