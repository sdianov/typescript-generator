package org.bitbucket.sdianov.tsgen.elements;

public class TSUnit extends TSContainer {

    public TSUnit() {
        super(null, "");
    }

    public TSImport addImport(String module, String... typeNames) {
        TSImport result = new TSImport(this, module, typeNames);

        elements.add(result);
        return result;
    }

    public TSInterface addInterface(String name) {

        TSInterface result = new TSInterface(this, name);

        elements.add(result);
        return result;
    }

    public TSClass addClass(String name) {

        TSClass result = new TSClass(this, name);

        elements.add(result);
        return result;
    }

    public TSConst addConst(String name, String value) {
        TSConst result = new TSConst(this, name, value);

        elements.add(result);
        return result;
    }
}
