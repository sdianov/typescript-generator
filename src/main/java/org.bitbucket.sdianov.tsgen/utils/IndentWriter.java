package org.bitbucket.sdianov.tsgen.utils;

import com.google.common.base.Strings;

public class IndentWriter {

    private final static int INDENT_SPACES = 4;

    private StringBuilder builder;

    public IndentWriter(StringBuilder builder) {
        this.builder = builder;
    }

    private int currentLevel = 0;

    private String indent(int level) {
        return Strings.repeat(" ", level * INDENT_SPACES);
    }

    public void writeLine(String format, Object... parameters) {
        builder.append(indent(currentLevel)).append(String.format(format, parameters)).append("\n");
    }

    public void writeBlockStart(String format, Object... parameters) {
        builder.append(indent(currentLevel)).append(String.format(format, parameters)).append("\n");
        currentLevel += 1;
    }

    public void writeBlockEnd(String format, Object... parameters) {
        currentLevel -= 1;
        builder.append(indent(currentLevel)).append(String.format(format, parameters)).append("\n");

    }


}
