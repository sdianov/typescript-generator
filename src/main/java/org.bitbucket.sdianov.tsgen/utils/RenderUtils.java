package org.bitbucket.sdianov.tsgen.utils;

import java.util.Collection;
import java.util.Collections;
import java.util.function.Function;
import java.util.stream.Collectors;

public class RenderUtils {

    public static <T> String commaJoin(Collection<T> elements, Function<T, String> function) {
        return elements.stream().map(function).collect(Collectors.joining(", "));
    }

    public static <T> String pipeJoin(Collection<T> elements, Function<T, String> function) {
        return elements.stream().map(function).collect(Collectors.joining(" | "));
    }

    public static String triangleBrace(String text) {
        return String.format("<%s>", text);
    }

    public static <T> String spaceJoin(Collection<T> elements, Function<T, String> function) {
        return elements.stream().map(function).collect(Collectors.joining(" "));
    }
}
