import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
export const BASE_URL='https://site.org';
export interface IParameters {
    id: number;
    name: string;
    roles?: string[];
}
@Injectable()
export class ServiceClient {
    constructor (private _http: Http) {
    }
    callService(params: IParameters): Observable<Response> {
        return this._http.get(`${BASE_URL}/api/service/${params.id}`)
    }
}